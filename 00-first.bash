#! /usr/bin/env bash
set -uvx
set -e

opt="--needed --noconfirm --disable-download-timeout"

#To update System
sudo pacman -Syyu
#Installing Driver and related utilites
sudo pacman -S $opt nvidia nvidia-utils nvidia-settings
#VirtualBox Drivers
sudo pacman -S $opt virtualbox-guest-utils 
# Installing Xorg
sudo pacman -S $opt xorg xterm xorg-xinit
startx

