#! /usr/bin/env bash
set -uvx
set -e

# https://wiki.manjaro.org/index.php/VirtualBox

#~/arch/manjaro  mhwd-kernel -li                                                                                                                                                  ✔
#Currently running: 5.15.60-1-MANJARO (linux515)
#The following kernels are installed in your system:
#   * linux515

opt="--needed --noconfirm --disable-download-timeout"

sudo pacman -Syu $opt virtualbox linux515-virtualbox-host-modules
#sudo vboxreload

sudo modprobe vboxguest vboxvideo vboxsf
sudo systemctl enable --now vboxservice.service
