#! /usr/bin/env bash
set -uvx
set -e

opt="--needed --noconfirm --disable-download-timeout"

sudo pacman -S $opt gnome gnome-extra
sudo pacman -S $opt pulseaudio pulseaudio-alsa pavucontrol
sudo pacman -S $opt gnome-terminal gnome-system-monitor
#sudo pacman -S $opt firefox vlc audacious
sudo pacman -S $opt firefox
#sudo pacman -S $opt libreoffice

sudo pacman -S $opt sddm
sudo systemctl enable sddm
sudo systemctl start sddm
